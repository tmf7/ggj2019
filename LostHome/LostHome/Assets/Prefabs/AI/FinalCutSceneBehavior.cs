﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class FinalCutSceneBehavior : MonoBehaviour
{
    [SerializeField] private Transform[] path;
    [SerializeField] private LayerMask waypointLayer;

    private NavMeshAgent agent;
    private float m_agentBaseSpeed;
    private int m_pathIndex = 0;

    private Animator m_animator;
    private int m_speedParameter = Animator.StringToHash("Speed");

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        m_agentBaseSpeed = agent.speed;

        if (path.Length > 0)
        {
            agent.destination = path[m_pathIndex].position;
        }
    }

    private void Update()
    {
        m_animator.SetFloat(m_speedParameter, Mathf.Clamp01(agent.velocity.magnitude));
    }

    private IEnumerator Meeting()
    {
        agent.isStopped = true;
        agent.speed = 0.45f;
        yield return new WaitForSeconds(7.0f);
        agent.isStopped = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.layer & (int)Mathf.Log(waypointLayer.value, 2)) != 0)
        {
            if (m_pathIndex < path.Length - 1)
            {
                m_pathIndex++;
                agent.destination = path[m_pathIndex].position;
                StartCoroutine(Meeting());
            }
        }
    }
}
