﻿using UnityEngine;

public class BillboardToCamera : MonoBehaviour
{
    private Camera m_mainCamera;

    private void Start()
    {
        m_mainCamera = Camera.main;
    }

    void Update()
    {
        transform.LookAt(m_mainCamera.transform.position);
    }
}
