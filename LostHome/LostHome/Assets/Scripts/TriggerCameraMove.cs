﻿using UnityEngine;

public class TriggerCameraMove : MonoBehaviour
{
    [SerializeField] private LayerMask m_playerLayer;
    [SerializeField] private CameraController.Direction m_triggerDirection = CameraController.Direction.NORTH;

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.layer == (int)Mathf.Log(m_playerLayer.value, 2)))
        {
            other.gameObject.GetComponent<Character>().FollowingCamera.SetCameraFacing(m_triggerDirection);
        }
    }
}
