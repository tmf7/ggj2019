﻿using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private float m_turnSpeed = 180.0f;
    [SerializeField] private float m_walkSpeed = 2.0f;
    [SerializeField] private float m_runSpeed = 4.0f;
    [SerializeField] private float m_personalGravity = 20.0f;

    private CharacterController m_controller;
    private Animator m_animator;
    private Vector3 m_moveDirection = Vector3.zero;
    private Camera m_mainCamera;

    private int m_speedParameter = Animator.StringToHash("Speed");
    private int m_fearParameter = Animator.StringToHash("Fear");
    private int m_actionParameter = Animator.StringToHash("Action");

    public float Speed
    {
        get
        {
            return m_animator.GetFloat(m_speedParameter);
        }
    }

    public CameraController FollowingCamera
    {
        get
        {
            return m_mainCamera.gameObject.GetComponent<CameraController>();
        }
    }

    public void SetFear(float newFear)
    {
        m_animator.SetFloat(m_fearParameter, Mathf.Clamp01(newFear));
    }

    void Start()
    {
        m_mainCamera = Camera.main;
        m_animator = GetComponent<Animator>();
        m_controller = GetComponent<CharacterController>();
        //m_controller.detectCollisions = true;
    }

    void Update()
    {
        float xMove = Input.GetAxis("Horizontal");
        float zMove = Input.GetAxis("Vertical");

        // TODO: horizontal should always translate to perp to camera
        // and vertical should always translate to away/towards camera
        // (not just along world xz axes)

        m_moveDirection = m_mainCamera.transform.TransformDirection(new Vector3(xMove, 0.0f, zMove));
        m_moveDirection = Vector3.ProjectOnPlane(m_moveDirection, Vector3.up);
        m_animator.SetFloat(m_speedParameter, m_moveDirection.magnitude);
        float groundSpeed = Mathf.Lerp(m_walkSpeed, m_runSpeed, m_moveDirection.magnitude);

        if (m_moveDirection != Vector3.zero)
        {
            if (Vector3.Angle(transform.forward, m_moveDirection) > 179)
            {
                // always turn to the right to go the opposite direction
                m_moveDirection = transform.TransformDirection(new Vector3(0.01f, 0.0f, -1.0f));
            }

            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(m_moveDirection), m_turnSpeed * Time.deltaTime);
        }

        m_moveDirection = m_mainCamera.transform.TransformDirection(new Vector3(xMove, 0.0f, zMove));
        m_moveDirection = Vector3.ProjectOnPlane(m_moveDirection, Vector3.up);
        m_moveDirection.y -= (m_personalGravity * Time.deltaTime);
        CollisionFlags flags = m_controller.Move(m_moveDirection * groundSpeed * Time.deltaTime);

        if (Input.GetButtonDown("Fire1") && flags == CollisionFlags.CollidedSides)
        {

            Ray ray = new Ray(m_controller.center, transform.forward);
            RaycastHit hitInfo;

            // TODO: alternatively to a spherecast
            if (m_controller.Raycast(ray, out hitInfo, float.MaxValue))
            {
                //hitInfo.collider.gameObject; // TODO: get a component attached to the hit item
            }

            m_animator.SetTrigger(m_actionParameter);
        }
    }
}

