﻿using UnityEngine;

public class MenuFlashlight : MonoBehaviour
{
    [SerializeField] private Canvas m_canvas;
    [SerializeField] private RectTransform m_flashlightRectTransform;

    private void OnMouseEnter()
    {
        Cursor.visible = false;
        m_flashlightRectTransform.gameObject.SetActive(true);
    }

    private void OnMouseExit()
    {
        Cursor.visible = true;
        m_flashlightRectTransform.gameObject.SetActive(false);
    }

    private void Update()
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(m_canvas.transform as RectTransform, Input.mousePosition, m_canvas.worldCamera, out pos);
        m_flashlightRectTransform.position = m_canvas.transform.TransformPoint(pos);
    }
}
