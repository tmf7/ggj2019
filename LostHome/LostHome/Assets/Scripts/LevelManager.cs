﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    [SerializeField] private CanvasGroup m_fadeCanvasGroup;
    [SerializeField] private AnimationCurve m_fadeCurve;

    public enum Levels : int
    {
        MAINMENU,
        FIRST_CUTSCENE,
        MOM_LEVEL,
        SECOND_CUTSCENE,
        CHILD_LEVEL,
        FINAL_CUTSCENE
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        LoadScene(Levels.MAINMENU);
    }

    public void MomLevel()
    {
        LoadScene(Levels.MOM_LEVEL);
    }

    public void ChildLevel()
    {
        LoadScene(Levels.CHILD_LEVEL);
    }

    public void FirstCutScene()
    {
        // artwork
        LoadScene(Levels.FIRST_CUTSCENE);
    }

    public void SecondCutScene()
    {
        // artwork
        LoadScene(Levels.SECOND_CUTSCENE);
    }

    public void FinalCutScene()
    {
        // static camera tableau walk into town
        LoadScene(Levels.FINAL_CUTSCENE);
    }

    public void CurrentScene()
    {
        LoadScene((Levels)SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadScene(Levels level)
    {
        Instance.StartCoroutine(Instance.FadeToWhite((int)level));
    }

    private IEnumerator FadeToWhite(int buildIndex)
    {
        float fadeDuration = 0.0f;
        bool loadingScene = false;
        
        while (fadeDuration <= 1.0f)
        {
            m_fadeCanvasGroup.alpha = m_fadeCurve.Evaluate(Mathf.Clamp01(fadeDuration += Time.deltaTime));

            if (!loadingScene && fadeDuration >= 0.5f)
            {
                SceneManager.LoadScene(buildIndex);
                loadingScene = true;
            }

            yield return null;
        }
    }
}
