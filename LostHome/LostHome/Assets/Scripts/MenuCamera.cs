﻿using UnityEngine;

public class MenuCamera : MonoBehaviour
{
    [SerializeField] private float m_moveSpeed = 10.0f;
    [SerializeField] private float m_rotationSpeed = 90.0f;
    [SerializeField] private Transform m_cameraTransform;
    [SerializeField] private Transform m_mainMenuCameraPosition;
    [SerializeField] private Transform m_controlsCameraPosition;
    [SerializeField] private Transform m_creditsCameraPosition;

    private Transform m_moveTarget;

    private void Awake()
    {
        m_moveTarget = m_mainMenuCameraPosition;
    }

    public void MoveToMainMenu()
    {
        m_moveTarget = m_mainMenuCameraPosition;
    }

    public void MoveToCredits()
    {
        m_moveTarget = m_creditsCameraPosition;
    }

    public void MoveToControls()
    {
        m_moveTarget = m_controlsCameraPosition;
    }

    private void Update()
    {
        m_cameraTransform.position = Vector3.MoveTowards(m_cameraTransform.position, m_moveTarget.position, m_moveSpeed * Time.deltaTime);
        m_cameraTransform.rotation = Quaternion.RotateTowards(m_cameraTransform.rotation, m_moveTarget.rotation, m_rotationSpeed * Time.deltaTime);
    }
}
