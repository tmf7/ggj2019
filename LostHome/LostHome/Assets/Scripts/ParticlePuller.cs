﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ParticlePuller : MonoBehaviour
{


    //Holds Fog Systems
    public Transform[] Fogs;
    private List<Transform> calledFog = new List<Transform>();

    [SerializeField] float maxSqr;
    [SerializeField] float minSqr;
    [SerializeField] float speed;

    bool canMove = false;
    bool recallScene = false;
    bool canSummon = true;
    bool isWaiting = false;

    private void Start()
    {

        canMove = false;
        recallScene = false;
        canSummon = true;
        isWaiting = false;
        recallScene = false;
    }

    private void Update()
    {
        //TEST
        if (Input.GetKeyDown(KeyCode.K))
        {
            Debug.Log("Click");
            SummonFog();
            canSummon = false;
        }

        if (recallScene)
        {
            Debug.Log("Reload");
            //LevelManager.Instance.CurrentScene();
        }

        if (canMove)
            MoveFog();
    }


    /// <summary>
    /// Checks All ParticleSystems in Fogs Array IF within distance params
    /// </summary>
    public void SummonFog()
    {
        //Death Condition
        Debug.Log("FogSummoned");
        for (int i = 0; i < Fogs.Length; i++)
        {
            //float DistanceSqr = (transform.position - Fogs[i].position).sqrMagnitude;

           // if (DistanceSqr < maxSqr && DistanceSqr > minSqr)
           // {
                Debug.Log("Adding to Array");
                calledFog.Add(Fogs[i]);

                // }
            }

            canMove = true;
        }

        //CALL IN UPDATE after Summon Fog
        void MoveFog()
        {

            for (int i = 0; i < calledFog.Count; i++)
            {
                calledFog[i].gameObject.SetActive(true);
                calledFog[i].LookAt(transform.position);
                calledFog[i].Translate(Vector3.forward * Time.deltaTime * speed);

                Debug.Log(Vector3.Distance(transform.position, calledFog[i].position));

                if (Vector3.Distance(transform.position, calledFog[i].position) <= 15f)
                {
                    calledFog.Remove(calledFog[i]);

                    if (calledFog.Count == 0 && !recallScene)
                    {
                        canMove = false;
                        recallScene = true;
                        LevelManager.Instance.CurrentScene();
                    }
                }

            }


        }

    }
