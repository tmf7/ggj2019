﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EditTreeGood : MonoBehaviour
{
    public GameObject[] newTrees;

    Transform[] Children;
    Transform Model;

    // Start is called before the first frame update
    void Awake()
    {
        //Debug.Log("Start");
        Children = new Transform[transform.childCount];

        for (int i = 0; i < Children.Length; i++)
        {

            //Debug.Log(i);
            Children[i] = transform.GetChild(i);
            Model = Children[i].GetChild(0);

           // Debug.Log("Model Name " + Model.name);
            GameObject newT = Instantiate(newTrees[Random.Range(0, 2)], Model.transform.position, Model.transform.rotation);
            newT.transform.parent = Model.transform;
            newT.transform.localEulerAngles = new Vector3(transform.rotation.x - 90f, transform.rotation.y, transform.rotation.z);
            newT.transform.localScale =  new Vector3(transform.localScale.x * .3f, transform.localScale.y * .3f, transform.localScale.z * .6f); 
            Model.GetChild(0).gameObject.SetActive(false);
        }

    }

}
