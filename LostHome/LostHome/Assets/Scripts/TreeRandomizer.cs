﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeRandomizer : MonoBehaviour
{
    Transform[] Children;

    // Start is called before the first frame update
    void Start()
    {

        Children = new Transform[transform.childCount];

       for(int i = 0; i < transform.childCount; i++)
        {
            Children[i] = transform.GetChild(i);
        }

       foreach(Transform t in Children)
        {
            float f = Random.Range(2f, 3f);
            t.localScale = new Vector3(f, f, f);

            //float r = Random.Range(-5, 10);
            //t.localEulerAngles = new Vector3(0f, r, 0f);
        }
    }
}
