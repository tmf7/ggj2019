﻿using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager Instance { get; private set; }

    [SerializeField] private DialogPopup m_dialogPopupPrefab;
    [SerializeField] private float m_dialogHeight = 1.5f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // TODO: call this either on a timer, and/or at trigger points
    public void PlayDialogue(AudioClip voiceClip, string text, Transform target, bool changeLevel = false, LevelManager.Levels newLevel = LevelManager.Levels.MAINMENU)
    {
        DialogPopup newPopup = Instantiate(m_dialogPopupPrefab, target, true);
        newPopup.transform.position = target.position + Vector3.up * m_dialogHeight; // above their head
        newPopup.gameObject.SetActive(false);
        newPopup.PlayDialogue(voiceClip, text, changeLevel, newLevel); // re-activates itself
    }
}
