﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class CutSceneAudio : MonoBehaviour
{
    [SerializeField] private AudioSource[] m_audioSources;
    [SerializeField] private LevelManager.Levels m_nextLevel;
    [SerializeField] private AudioMixer m_volumeMixer;

    private void Awake()
    {
        StartCoroutine(BeginSequence());
    }

    private IEnumerator BeginSequence()
    {
        for (int i = 0; i < m_audioSources.Length; ++i)
        {
            if (i > 0 && m_nextLevel == LevelManager.Levels.MAINMENU)
            {
                m_audioSources[i - 1].gameObject.SetActive(false);
            }

            m_audioSources[i].gameObject.SetActive(true); // all set to playOnAwake

            if (m_nextLevel != LevelManager.Levels.MAINMENU)
            {
                yield return new WaitForSeconds(m_audioSources[i].clip.length - 0.75f);
            }
            else
            {
                yield return new WaitUntil(() => m_audioSources[i].isPlaying == false);
                m_audioSources[i].gameObject.SetActive(false);
            }
        }

        if (m_nextLevel == LevelManager.Levels.MAINMENU)
        {
            yield return new WaitForSeconds(4.0f);
            float fadeOut = 15.0f;
            while (fadeOut > 0.0f)
            {
                float musicVolume;
                m_volumeMixer.GetFloat("Attenuation", out musicVolume);
                fadeOut -= Time.deltaTime;
                m_volumeMixer.SetFloat("Attenuation", Mathf.MoveTowards(musicVolume, -80.0f, 1.0f * Time.deltaTime));
                yield return null;
            }

            LevelManager.Instance.LoadScene(m_nextLevel);
        }
        else
        {
            LevelManager.Instance.LoadScene(m_nextLevel);
        }
    }
}
