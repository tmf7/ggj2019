﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static SoundManagerScript Instance { get; private set; }
    public AudioSource AudioOutput; 
    public AudioSource SFXOutput;
    public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.


    void Awake()
    {
        //Check if there is already an Instance of SoundManager
        if (Instance == null)
            //if not, set it to this.
            Instance = this;
        //If Instance already exists:
        else if (Instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one Instance of SoundManager.
            Destroy(gameObject);
         
        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }


    //Used to play single sound clips.
    public void playSound(AudioClip recievedSFXClip)
    {
        Debug.Log("Music Played");
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        SFXOutput.clip = recievedSFXClip;

        //Play the clip.
        SFXOutput.Play();
    }

    public void playMusic(AudioClip recievedMusicClip)
    {

        
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        AudioOutput.clip= recievedMusicClip;

        //Play the clip.
        AudioOutput.Play();
    }

    public void playVoice(AudioClip recievedVoiceClip)
    {


        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        AudioOutput.clip = recievedVoiceClip;

        //Play the clip.
        AudioOutput.Play();
    }

}