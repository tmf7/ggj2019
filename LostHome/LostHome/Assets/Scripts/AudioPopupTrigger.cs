﻿using UnityEngine;
using System.Collections;

public class AudioPopupTrigger : MonoBehaviour
{
    [SerializeField] private AudioClip m_audioClip;
    [SerializeField] private string m_text;
    [SerializeField] private LayerMask m_targetLayer;
    [SerializeField] private bool m_changeLevelOnFinish = false;
    [SerializeField] private LevelManager.Levels m_nextLevel = LevelManager.Levels.MAINMENU;
 
    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.layer == (int)Mathf.Log(m_targetLayer.value, 2)))
        {
            DialogueManager.Instance.PlayDialogue(m_audioClip, m_text, other.transform, m_changeLevelOnFinish, m_nextLevel);
            gameObject.SetActive(false); // dont re-trigger
        }
    }
}
