﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomSpookySounds : MonoBehaviour
{
    public static RandomSpookySounds Instance { get; private set; }
    [SerializeField] private AudioClip[] spookyClips;
    [SerializeField] private float coolDownDuration = 60.0f;

    private bool locked;

    private void OnTriggerEnter(Collider other)
    {
        //Make Sure Player Tag is enabled on the player controller
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("SpookyTrap Triggered");
            int rndAudioIndex = Random.Range(0, spookyClips.Length);
            sendSoundEffects(spookyClips[rndAudioIndex]);
        }
    }

    public void idleMovementSpookyNoise()
    {
        int rndIdleAudioIndex = Random.Range(0, spookyClips.Length);
        sendSoundEffects(spookyClips[rndIdleAudioIndex]);
    }

    public void sendSoundEffects(AudioClip clip)
    {
        if (!locked)
        {
            Debug.Log("send sound effect function triggered");
            SoundManagerScript.Instance.playSound(clip);
            StartCoroutine(coolDown());
        }
    }

   private IEnumerator coolDown()
    {
        locked = true;
        yield return new WaitForSeconds(coolDownDuration);
        locked = false;
    }
}
