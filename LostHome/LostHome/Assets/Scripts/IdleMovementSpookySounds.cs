﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleMovementSpookySounds : MonoBehaviour
{
    
    [SerializeField] private AudioClip[] spookyClips;
    [SerializeField] private float coolDownDuration = 2.0f;
    [SerializeField] private Character childCharacter;

    private IEnumerator cooldownNoise;

    // Update is called once per frame
    void Update()
    {
        if(childCharacter.Speed <= Mathf.Epsilon && cooldownNoise == null)
        {
            cooldownNoise = idleSoundBreakDuration();
            StartCoroutine(cooldownNoise);
        }
    }

    public void sendSoundEffects(AudioClip clip)
    {
        Debug.Log("send sound effect function triggered");
        SoundManagerScript.Instance.playSound(clip);
    }

    private IEnumerator idleSoundBreakDuration()
    {
        yield return new WaitForSeconds(coolDownDuration);
       Debug.Log("SpookyTrap Triggered");
            int rndAudioIndex = Random.Range(0, spookyClips.Length);
            sendSoundEffects(spookyClips[rndAudioIndex]);
            cooldownNoise = null;
    }
}
