﻿using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class GhostGirl : MonoBehaviour
{
    [SerializeField] private AudioClip[] m_screams;
    [SerializeField] private ParticlePuller m_fogParticles;
    [SerializeField] private CameraController m_cameraController;
    [SerializeField] private Transform[] path;
    [SerializeField] private LayerMask waypointLayer;
    [SerializeField] private Transform mommy;
    [SerializeField] private float m_tooCloseThreshold = 2.0f;
    [SerializeField] private float m_maxDistanceFromMom = 10.0f;
    [SerializeField] private float m_fearSpeed = 0.33f;

    private NavMeshAgent agent;
    private float m_agentBaseSpeed;
    private int m_pathIndex = 0;

    private Animator m_animator;
    private int m_speedParameter = Animator.StringToHash("Speed");
    private int m_fearParameter = Animator.StringToHash("Fear");
    private int m_actionParameter = Animator.StringToHash("Action");

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        m_agentBaseSpeed = agent.speed;

        if (path.Length > 0)
        {
            agent.destination = path[m_pathIndex].position;
        }
    }

    private void Update()
    {
        float rangeToMom = Vector3.Distance(transform.position, mommy.position);

        agent.speed = m_agentBaseSpeed * (m_maxDistanceFromMom / rangeToMom);
        m_animator.SetFloat(m_speedParameter, Mathf.Clamp01(agent.velocity.magnitude));

        if (rangeToMom < m_tooCloseThreshold && m_pathIndex > 0)
        {
            int randomIndex = Random.Range(0, m_screams.Length);
            m_fogParticles.SummonFog();
            SoundManagerScript.Instance.playSound(m_screams[randomIndex]);
            mommy.GetComponent<Character>().FollowingCamera.SetTarget(false);
            m_animator.SetFloat(m_fearParameter, 1.0f);
            agent.isStopped = true;
            enabled = false;
        }
        else if (rangeToMom > m_maxDistanceFromMom)
        {
            agent.isStopped = true;
            m_animator.SetFloat(m_fearParameter, Mathf.Clamp01(m_animator.GetFloat(m_fearParameter) + m_fearSpeed * Time.deltaTime));
        }
        else
        {
            agent.isStopped = false;
            m_animator.SetFloat(m_fearParameter, 0.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.layer & (int)Mathf.Log(waypointLayer.value, 2)) != 0)
        {
            if (m_pathIndex < path.Length - 1)
            {
                m_pathIndex++;
                agent.destination = path[m_pathIndex].position;
            }
        }
    }
}
