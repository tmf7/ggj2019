﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ScriptToVoiceOver : MonoBehaviour
{

    // TODO: make an animation curve to instantiate the worldspace dialog prefab and set its text (leave disabled until text is set so it types out)

    [Serializable]
    public struct VoicedText
    {
        public string text;
        public AudioClip voiceOver;
        public bool canBeRandom;
    }

    //[SerializeField] private JuicyGraphic m_targetGraphic;
    [SerializeField] private Text m_targetText;
    [SerializeField] private AudioSource m_audioSource;
    [SerializeField] private List<VoicedText> m_voicedText = new List<VoicedText>();

    public bool IsPlaying => m_audioSource.isPlaying;

    private int playIndex = 0;

    private void Awake()
    {
        m_audioSource.loop = false;
        m_audioSource.playOnAwake = false;
        StartCoroutine(PlayScript());
    }

    private IEnumerator PlayScript()
    {
        while (true)
        {
            yield return null;
            yield return new WaitUntil(() => m_audioSource.isPlaying == false);
            PlayNext();
        }
    }

    // for playing scripted sequences
    public void PlayNext()
    {
        // assumes randoms are added at end of scripted voiceover
        if (playIndex < m_voicedText.Count - 1)
        {
            ShowAndTell(m_voicedText[playIndex]);
            playIndex++;
        }
        else if (m_voicedText.Any(voiceOver => voiceOver.canBeRandom))
        {
            PlayRandom();
        }
    }

    // for playing random ui popup prompts
    public void PlayRandom()
    {
        int MAX_ATTEMPTS = 20;
        int randomIndex = 0;

        do
        {
            randomIndex = Random.Range(0, m_voicedText.Count);
            MAX_ATTEMPTS--;
        } while (!m_voicedText[randomIndex].canBeRandom || MAX_ATTEMPTS > 0);

        ShowAndTell(m_voicedText[randomIndex]);
    }

    private void ShowAndTell(VoicedText target)
    {
        m_audioSource.clip = m_voicedText[playIndex].voiceOver;
        m_audioSource.Play();
        m_targetText.text = m_voicedText[playIndex].text;
    }
}
