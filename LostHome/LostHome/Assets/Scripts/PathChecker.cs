﻿using UnityEngine;
using System.Linq;

public class PathChecker : MonoBehaviour
{
    [SerializeField] private LayerMask m_triggerPathLayer;
    //[SerializeField] private GameObject playerRef;
    [SerializeField] private ParticlePuller particles;

    private int m_onPathTriggers = 0;

    private void Awake()
    {
        particles = FindObjectOfType<ParticlePuller>();
    }

    //private void OnTriggerExit(Collider other)
    //{

    //    if (other.gameObject.tag == playerRef.tag)
    //    {
    //        m_onPathTriggers--;
    //        Debug.Log("player left");

    //        if (m_onPathTriggers <= 0)
    //        {
    //            particles.SummonFog();
    //        }
    //    }
    //}

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == playerRef.tag)
    //    {
    //        Debug.Log("player entered");
    //        m_onPathTriggers++;
    //    }
    //}

    private void Update()
    {
        if (!Physics.OverlapSphere(transform.position, 3.0f).Any(collider => collider.gameObject.layer == (int) Mathf.Log(m_triggerPathLayer, 2)))
        {
            particles.SummonFog();
        }
    }
}
