﻿using UnityEngine;
using UnityEngine.UI;

public class DialogPopup : MonoBehaviour
{
    [SerializeField] private AnimationCurve m_scaleCurve;
    [SerializeField] private AudioSource m_audioSource;
    [SerializeField] private Text m_text;

    private float m_initialScale;
    private float m_scaleTime;

    private bool m_changeLevelOnFinish = false;
    private LevelManager.Levels m_nextLevel = LevelManager.Levels.MAINMENU;

    private void Awake()
    {
        m_initialScale = Mathf.Abs(transform.localScale.y);
        // audiosource set to play on awake
    }

    private void Update()
    {
        m_scaleTime += Time.deltaTime;
        transform.localScale = m_scaleCurve.Evaluate(m_scaleTime) * m_initialScale * new Vector3(-1.0f, 1.0f, 1.0f);

        if (m_scaleTime > 5.0f) // hardcoded (no clip longer than 5 seconds
        {
            if (m_changeLevelOnFinish)
            {
                LevelManager.Instance.LoadScene(m_nextLevel);
            }
            Destroy(gameObject);
        }
    }

    public void PlayDialogue(AudioClip voiceClip, string text, bool changeLevel, LevelManager.Levels newLevel)
    {
        m_audioSource.clip = voiceClip;
        m_text.text = text;
        gameObject.SetActive(true); // instantiates disabled to give time for this setup
        m_changeLevelOnFinish = changeLevel;
        m_nextLevel = newLevel;
    }
}
