﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public enum Direction : int
    {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }

    [SerializeField] private Transform m_focusTarget;
    [SerializeField] private Transform m_primaryTarget;
    [SerializeField] private Transform m_secondaryTarget;

    [SerializeField] private Vector3 m_targetRange = new Vector3(0.0f, 2.0f, -3.0f);
    [SerializeField] private float m_smoothTime = 0.3f;
    [SerializeField] private Direction m_initialDirection = Direction.NORTH;

    private Vector3 m_focusRange;
    private Vector3 m_velocity = Vector3.zero;

    private void Awake()
    {
        SetCameraFacing(m_initialDirection);
    }

    private void Update()
    {
        // never rotate the camera, only translate on xz plane
        transform.position = Vector3.SmoothDamp(transform.position, m_focusTarget.position + m_focusRange, ref m_velocity, m_smoothTime);

        Vector3 cameraDirection = m_focusTarget.position - transform.position;
        transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(cameraDirection, Vector3.up));
    }

    public void SetTarget(bool isPrimaryTarget)
    {
        if (isPrimaryTarget)
        {
            m_focusTarget = m_primaryTarget;
        }
        else
        {
            m_focusTarget = m_secondaryTarget;
        }
    }

    public void SetCameraFacing(Direction newDir)
    {
        switch (newDir)
        {
            case Direction.NORTH: m_focusRange = new Vector3(m_targetRange.x, m_targetRange.y, 0.0f); break;
            case Direction.SOUTH: m_focusRange = new Vector3(-m_targetRange.x, m_targetRange.y, 0.0f); break;
            case Direction.EAST: m_focusRange = new Vector3(0.0f, m_targetRange.y, m_targetRange.x); break;
            case Direction.WEST: m_focusRange = new Vector3(0.0f, m_targetRange.y, -m_targetRange.x); break;
        }
    }
}
